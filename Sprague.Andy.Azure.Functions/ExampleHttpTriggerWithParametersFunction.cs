using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Sprague.Andy.Azure.Contracts;

namespace Sprague.Andy.Azure.Functions
{
    public static class ExampleHttpTriggerWithParametersFunction
    {
        private const string FunctionName = nameof(ExampleHttpTriggerWithParametersFunction);
        private const string Route = "analysis/{requestUid}";

        [FunctionName(FunctionName)]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = Route)] HttpRequestMessage req,
            string requestUid,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function called with RequestUid={RequestUid}", requestUid);
            try
            {
                var uid = Guid.Parse(requestUid);
                var content = await req.Content.ReadAsAsync<TestRequest>();
                log.LogInformation("Request Content:" + content);

                var response = new TestResponse
                {
                    RequestUid = uid,
                    Query = content.Query,
                };

                var res = req.CreateResponse(HttpStatusCode.OK, response);
                return res;
            }
            catch (Exception ex)
            {
                log.LogError("{FunctionName} request failed: {Error}", FunctionName, ex.Message);
                return req.CreateResponse(HttpStatusCode.BadRequest, $"Request failed for route {Route}: " + ex);
            }

        }

        

    }

   

}