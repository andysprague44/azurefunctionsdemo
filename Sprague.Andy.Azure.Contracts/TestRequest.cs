﻿using System;

namespace Sprague.Andy.Azure.Contracts
{
    public class TestRequest
    {
        public string Query { get; set; }

        public override string ToString()
        {
            return $"{nameof(Query)}: {Query}";
        }
    }
}
