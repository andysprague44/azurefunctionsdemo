﻿using System;

namespace Sprague.Andy.Azure.Contracts
{
    public class TestResponse
    {
        public Guid RequestUid { get; set; }
        public string Query { get; set; }
    }
}
